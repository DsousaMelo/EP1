EP1 - Orientação a Objetos - 2018/1
Aluno: Daniel de Sousa Oliveira Melo Veras - matrícula: 17/0008371.

Como utilizar o programa:

1. Faça o download dos arquivos disponibilizados.

2. Após extrair os arquivos, deve-se digitar o comando "make" para compilar e "make run" para que o programa seja iniciado.

3. As imagens utilizadas como exemplos estão contidas na pasta do projeto. Para cada criptografia é criado arquivos, dentro da pasta do projeto, de texto com a mensagem antes e depois da criptografia.
