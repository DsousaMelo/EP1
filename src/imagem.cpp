#include <iostream>
#include <fstream>
#include <string>
#include "imagem.hpp"

using namespace std;
Imagem::Imagem(){}
Imagem::~Imagem(){}
void Imagem::setWidth(int w){
    this->width = w;
}
int Imagem::getWidth(){
    return this->width;
}
void Imagem::setHeight(int h){
    this->height = h;
}
int Imagem::getHeight(){
    return this->height;
}
void Imagem::setCerquilha(char c){
    this->cerquilha = c;   
}
char Imagem::getCerquilha(){
    return this->cerquilha;
}
void Imagem::setTipo(string t){
    this->tipo = t;
}
string Imagem::getTipo(){
    return this->tipo;
}
void Imagem::setInicio(int i){
    this->inicio = i;
}
int Imagem::getInicio(){
    return this->inicio;
}
void Imagem::setCifraCesar(int ces){
    this->cifraCesar = ces;
}
int Imagem::getCifraCesar(){
    return this->cifraCesar;
}
void Imagem::setKey(string k){
    this->key = k;
}
string Imagem::getKey(){
    return this->key;
}
void Imagem::setTamanho(int tam){
    this->tamanho = tam;
}
int Imagem::getTamanho(){
    return this->tamanho;
}
void Imagem::setQtdepixel(int qtde){
    this->qtdepixel = qtde;
}
int Imagem::getQtdepixel(){
    return this->qtdepixel;
}
