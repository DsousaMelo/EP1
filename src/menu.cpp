#include "menu.hpp"
#include <iostream>
#include <string>

using namespace std;
Menu::Menu(){
	cout<<"                   <>===============================<>"<<endl;
	cout<<"                   ||         CRIPTOGRAMANDO        ||"<<endl;
	cout<<"                   <>===============================<>"<<endl;
	cout<<"                   <>===============================<>"<<endl;
	cout<<"                   ||NÓS SÓ PODEMOS VER UM POUCO DO ||"<<endl;
	cout<<"                   ||FUTURO, MAS O SUFICIENTE PARA  ||"<<endl;
	cout<<"                   ||PERCEBER QUE HÁ MUITO A FAZER  ||"<<endl;
	cout<<"                   ||                   ALAN TURING ||"<<endl;
	cout<<"                   <>===============================<>"<<endl;
	cout<<"                   <>===============================<>"<<endl;
	cout<<"\n";
	cout<<"<> Digite onde esta localizada a imagem para descobrir a mensagem secreta(Ex:/home/usuario/imagem/*.ppm): ";

}
Menu::~Menu(){}

void Menu::CriandoPPM_or_PGM(){

    cin >> this->nomeArquivo;
    cout<<"\n";

    int tam = nomeArquivo.length();
    	if (nomeArquivo[tam-2]== (char)112) //Criando objeto PPM
    	{
    		PPM *Img = new PPM(nomeArquivo);
    		Img->Lendo_Pixels();
    		Img->DescobrindoMensagem();
    		Img->Keyword();
    		Img->LiberarMemoria();
    		delete Img;

		}else if(nomeArquivo[tam-2]== (char)103){ //Criando objeto PGM
			PGM *Img = new PGM(nomeArquivo);
    		Img->Lendo_Pixels();
    		Img->DescobrindoMensagem();
    		Img->CesarCipher();
    		Img->LiberarMemoria();
    		delete Img;
    	}else{
    		throw "Dígite um caminho válido, a imagem deve ser do formato ppm ou pgm";
    	}
    
}
