#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <cctype>
#include "ppm.hpp"

using namespace std;

PPM::PPM(string nomeArquivo){
            this->Arquivo.open(nomeArquivo,ios::in);
}
PPM::~PPM(){}

void PPM::Lendo_Pixels() {

            struct Pixel **pixel;

            string w;
            string h;
            char cer;
            string tip;
            string ini;
            string k;
            string tam;
            string qtd;

            getline(Arquivo, tip, '\n');
            Arquivo.get(cer);
            getline(Arquivo, ini,' ');
            getline(Arquivo, tam,' ');
            getline(Arquivo, k,'\n');
            getline(Arquivo, w, ' ');
            getline(Arquivo, h, '\n');
            getline(Arquivo, qtd, '\n');


            int inicio_convert = stoi(ini);
            int width_convert = stoi(w);
            int height_convert = stoi(h);
            int tamanho_convert = stoi(tam);
            int qtd_convert = stoi(qtd);

            setInicio(inicio_convert);
            setWidth(width_convert);
            setHeight(height_convert);
            setTamanho(tamanho_convert);
            setKey(k);
            setCerquilha(cer);
            setTipo(tip);
            setQtdepixel(qtd_convert);

            pixel = (struct Pixel **) malloc (sizeof(struct Pixel *) * getHeight()); //alocando espaço em memoria para os pixels lidos no arquivo de entrada
            for (int i = 0; i < getWidth(); i++) {
                pixel[i] = (struct Pixel *) malloc (sizeof(struct Pixel) * getWidth());
            }

            for (int y = 0; y < getHeight(); y++) {
                for (int x = 0; x < getWidth(); x++) {
                    Arquivo.get(pixel[y][x].r);
                    Arquivo.get(pixel[y][x].g);
                    Arquivo.get(pixel[y][x].b);
                }
            }

            this->pixels = pixel;

            Arquivo.close(); 
        }

void PPM::DescobrindoMensagem(){
        
    somatot = (unsigned int *) malloc (sizeof(unsigned int) * getTamanho());
    unsigned int inicio_real = getInicio()/3;
    unsigned int divisao = (getInicio()%3);
    unsigned int pos_inicio = (inicio_real/getWidth());
    unsigned int linha_ini = inicio_real - (pos_inicio * getWidth());

        for (unsigned int i = pos_inicio; i <= pos_inicio ; i++)
        {
            int z=0;
         for (unsigned int j = linha_ini; j < linha_ini + getTamanho(); j++)
            {
                if(divisao == 1){
                    somatot[z] = ((unsigned int) pixels[i][j].g % 10)+((unsigned int) pixels[i][j].b % 10)+((unsigned int) pixels[i][j+1].r % 10);

                }else if(divisao == 2){
                    somatot[z] = ((unsigned int)pixels[i][j].b%10)+(( unsigned int)pixels[i][j+1].r%10)+((unsigned int)pixels[i][j+1].g%10);

                }else if(divisao == 0){
                somatot[z] = ((unsigned int)pixels[i][j].r%10)+(( unsigned int)pixels[i][j].g%10)+((unsigned int)pixels[i][j].b%10);
                
                }
                z++;
           }
           
        z--;
        }
        
    ofstream TextoCrip;
    TextoCrip.open("./TextosPPM/TextoCriptogradoNumeros.txt");
    for (int y = 0; y < getTamanho(); y++) {
        
            TextoCrip << somatot[y]<<" ";
    }
    TextoCrip.close();

}
void PPM::Keyword(){
    string alfa = "abcdefghijklmnopqrstuvwxyz";
    int tamAlfa = 26;
    string keyword = getKey();
    int tamanho_cifra = getTamanho();
    char message[tamanho_cifra]; 

    for (int i = 0; i < tamanho_cifra; ++i)
    {
        int number;
        number = somatot[i];
        message[i] = alfa[number-1];     
    }

    ofstream TextoDescrip;
    TextoDescrip.open("./TextosPPM/TexoCriptogradoLetra.txt");
    for (int y = 0; y < tamanho_cifra; y++) {
        
            TextoDescrip << message[y];
    }
    TextoDescrip.close();

    string temp = "";
    for (int i = 0; i < (int)keyword.length(); i++)
    {
        temp += keyword[i];
    }
    for (int i = 0; i < 26; i++)
    {
        temp+= (char)(i+97);
    }


    for (int i = 0; i < (int)temp.length(); i++)
    {
        bool found = false;
            for (int j = 0; j <(int)keyword.length(); j++)
            {
                if(temp[i]==keyword[j]){
                    found = true;
                    break;
                }
            }
            if (found == false)
            {
                keyword+=temp[i];
            }
            
    }

    string decryText = "";
    for (int i = 0; i < tamanho_cifra ; i++)
    {
        if (message[i] == 0)
        {
            decryText += " ";
        }else{
            int counter = 0;
            for (int j = 0; j < tamAlfa; j++)
            {
                if (message[i]==keyword[j])
                {
                    decryText +=alfa[counter];
                    break;
                }else{
                    counter ++;
                }

            }
        }
    }
    ofstream TextoDes;
    TextoDes.open("./TextosPPM/TextoDesCriptogrado.txt");
    for (int y = 0; y < tamanho_cifra; y++) {
        
            TextoDes<< decryText[y];
    }
    TextoDes.close();

    cout <<"A mensagem encontrada foi: "<<decryText<<"\n\n";
}
void PPM::LiberarMemoria(){
    for (int i = 0; i < getHeight(); ++i)
    {
        free(pixels[i]);
    }
    free(pixels);

    free(somatot);
}