#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>
#include "pgm.hpp"

using namespace std;
PGM::PGM(string nomeArquivo){
            this->Arquivo.open(nomeArquivo,ios::in);
}
PGM::~PGM(){}
void PGM::Lendo_Pixels() {


            char *pixel;

            string w;
            string h;
            char cer;
            string tip;
            string ini;
            string cif;
            string tam;
            string qtd;

            getline(Arquivo, tip, '\n');
            Arquivo.get(cer);
            getline(Arquivo, ini,' ');
            getline(Arquivo, tam,' ');
            getline(Arquivo, cif,'\n');
            getline(Arquivo, w, ' ');
            getline(Arquivo, h, '\n');
            getline(Arquivo, qtd, '\n');


            int inicio_convert = stoi(ini);
            int width_convert = stoi(w);
            int height_convert = stoi(h);
            int tamanho_convert = stoi(tam);
            int cifra_convert = stoi(cif);
            int qtd_convert = stoi(qtd);

            setInicio(inicio_convert);
            setWidth(width_convert);
            setHeight(height_convert);
            setTamanho(tamanho_convert);
            setCifraCesar(cifra_convert);
            setCerquilha(cer);
            setTipo(tip);
            setQtdepixel(qtd_convert);

            pixel = (char *) malloc (sizeof(char) * (getHeight()) * (getWidth())); //alocando espaço em memoria para os pixels lidos no arquivo de entrada


            for (int y = 0; y <(getHeight() * getWidth()); y++) {
                Arquivo.get(pixel[y]);
            }

            this->pixels = pixel; 
            Arquivo.close();
            
        }

void PGM::DescobrindoMensagem(){
        
        char *frase;
        int contador = 0;
        frase = (char *) malloc (sizeof(char) * getTamanho());

        for (int y = getInicio(); y <= (getInicio()  + getTamanho()); y++) {
               
                    frase[contador]=pixels[y];
                    contador++;
                }contador--;

    this->frasecript = frase;

    ofstream TextoCrip;
    TextoCrip.open("./TextosPGM/TextoCriptogrado.txt");
    for (int y = 0; y < getTamanho(); y++) {
        
            TextoCrip << frasecript[y];
    }
    TextoCrip.close();


}

void PGM::CesarCipher(){

    char aux;
    int i, chave, tam;

    chave = getCifraCesar();
    tam = getTamanho();

    char crip[tam];

    for (int i = 0; i < tam; i++)
            {
                crip[i] = frasecript[i];
            }        

    for(i = 0; crip[i] != '\0'; ++i){
        aux = crip[i];
        
        if(aux >= 'a' && aux <= 'z'){
            aux = aux - chave;
            
            if(aux < 'a'){
                aux = aux + 'z' - 'a' + 1;
            }
            
            crip[i] = aux;
        }
        else if (aux == ' ' || aux == '-'|| aux =='.')
        {
            crip[i] = aux;
        }
        else if(aux >= 'A' && aux <= 'Z'){
            aux = aux - chave;
            
            if(aux < 'A'){
                aux = aux + 'Z' - 'A' + 1;
            }
            
            crip[i] = aux;
        }
    }
    
    ofstream TextoDescrip;
    TextoDescrip.open("./TextosPGM/TextoDesCriptogrado.txt");
    for (int y = 0; y < tam; y++) {
        
            TextoDescrip << crip[y];
    }
    TextoDescrip.close();
    cout<<"A mensagem encontrada foi: ";
    
    for (int i = 0; i < tam; ++i)
    {
        cout<<crip[i];
    }

    cout<<"\n\n";

}
void PGM::LiberarMemoria(){
   free(pixels);
   free(frasecript);
}