#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>

using namespace std;

class Imagem {

    private:
        int width;
        int height;
        char cerquilha;
        string tipo;
        int inicio;
        int cifraCesar;
        string key;
        int tamanho;
        int qtdepixel;

    protected:
        ifstream Arquivo;
        

    public:
        Imagem();
        ~Imagem();
        int getWidth();
        int getHeight();
        char getCerquilha();
        string getTipo();
        int getInicio();
        int getCifraCesar();
        string getKey();
        int getTamanho();
        int getQtdepixel();
        void setWidth(int w);
        void setHeight(int h);
        void setCerquilha(char c);
        void setTipo(string t);
        void setInicio(int i);
        void setCifraCesar(int ces);
        void setKey(string k);
        void setTamanho(int tam);
        void setQtdepixel(int qtde);
};
#endif