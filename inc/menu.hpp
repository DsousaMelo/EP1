#ifndef MENU_HPP
#define MENU_HPP

#include <iostream>
#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"

using namespace std;

class Menu
{
	public:
		Menu();
		~Menu();
		void CriandoPPM_or_PGM();
		string nomeArquivo;	
};

#endif