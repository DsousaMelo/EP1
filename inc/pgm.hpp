#ifndef PGM_HPP
#define PGM_HPP

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include "imagem.hpp"

using namespace std;

class PGM: public Imagem {
	private:
		char *pixels;
		char *frasecript;

	public:
		PGM(string nomeArquivo);
		~PGM();
		void Lendo_Pixels();
		void Copy();
		void DescobrindoMensagem();
		void CesarCipher();
		void LiberarMemoria();
};
#endif