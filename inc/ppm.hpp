#ifndef PPM_HPP
#define PPM_HPP

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include "imagem.hpp"

using namespace std;
struct Pixel
{
	char r,g,b;
};

class PPM: public Imagem {
	private:
		
		struct Pixel **pixels;
		unsigned int *somatot;

	public:
		PPM(string nomeArquivo);
		~PPM();
		void Lendo_Pixels();
		void DescobrindoMensagem();
		void Keyword();
		void LiberarMemoria();
};
#endif